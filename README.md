# Mansion, Apartment, Shack, House #

### How do I set it up? ###
* In order to play the game, you need to download this repository onto your windows computer, and extract the files from App_Extract_Me_Here to the root folder of this repository.
* Then, you double click the Mash_with_MaterialUI Application to run.

### Why does this exist? ###

* I wanted to develop a game over the summer of 2020, so I decided to start small and make this. Turns out, this wasn't that small. 

### How do I play? ###

* Start the game after using the functional settings menu.
* Type a category and then four elements that fit that category. i.e. dream vacation, Italy, New Zealand, Puerto Rico, and Ireland.
* Once done, repeat this five more times, and then the game has been set up!
* Now, you just press continue and watch as different options start being crossed off, and then the results screen will appear.

### What now? ###

* Enjoy the game, let me know if there are any bugs or any ideas that you have for this repository!
* This was a fun simple project that I would recommend to anyone trying become better at coding or game development in general!

### Who do I talk to? ###

* The owner of this Repository is Eli Arnold, and you can send him an email at ArnoldEJ@jbu.edu